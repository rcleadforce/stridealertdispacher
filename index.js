/**
 * Response Concepts' Alert Message Dispacher to Stride rooms
 * Check Confluence documentation: 
 * https://response-concepts.atlassian.net/wiki/spaces/DOC/pages/371425355/Stride+Chat+Alerts+and+Notifications
 * Created by: Robert Szabo <robert@responseconcepts.com>
 */

var config = require('./config');
const port = config.port;
var strideApiUrl = config.strideApiUrl;
var strideSiteId = config.strideSiteId;
var strideRoomAccess = config.strideRoomAccess;
var strideAlertLevel = config.strideAlertLevel;

const log = require('simple-node-logger').createSimpleLogger('stride-messages.log');

const express = require('express');
const app = express();

var request = require('request');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function getStrideRoomId(room){
    if(typeof strideRoomAccess[room] == 'undefined' || strideRoomAccess[room] == null) throw Error ("The requested Room Name does not exist!");
    return strideRoomAccess[room].roomId;
}
function getStrideRoomBearer(room){
    if(typeof strideRoomAccess[room] == 'undefined' || strideRoomAccess[room] == null) throw Error ("The requested Room Name does not exist!");
    return strideRoomAccess[room].roomBearer;
}
function getMessagePanelType(level){
    if(typeof strideAlertLevel[level] == 'undefined' || strideAlertLevel[level] == null) throw Error ("The requested Error Level does not exist!");
    return strideAlertLevel[level].paneltype;
}
function getMessageTitle(level){
    if(typeof strideAlertLevel[level] == 'undefined' || strideAlertLevel[level] == null) throw Error ("The requested Error Level does not exist!");
    return strideAlertLevel[level].title;
}

// Routes for our API
// =============================================================================
var router = express.Router();

router.get('/', function(req, res) {
    res.statusCode = 404;
    res.json({ message: 'hooray! nothing to see here!' });
});

// Accessed at POST http://localhost:8080/api/stride-message)
// Expected data format: { "room": "roomname", "level": "error-level", "message": "message body" }
router.route('/stride-message')
    .post(function(req, res) {
        try{
            log.info('New request from ',req.ip, ' request body: ', JSON.stringify(req.body),' logged at ', new Date().toJSON());
            
            var msg = req.body["message"]; // TODO: validate content for security reasons

            var messageJSON = '{"version":1,"type":"doc","content":[{"type":"panel","attrs":{"panelType":"'+getMessagePanelType(req.body["level"])+'"},"content":[{"type":"paragraph","content":[{"type":"text","text":"'+getMessageTitle(req.body["level"])+'","marks":[{"type":"strong"}]},{"type":"text","text":"'+msg+'"}]}]}]}';
            sendStrideRoomMessage(getStrideRoomBearer(req.body["room"]), getStrideRoomId(req.body["room"]), messageJSON);
            
            res.statusCode = 200;
            res.send('Message sent.');
        } catch (e) {
            // console.log(e.name+': '+e.message); 
            log.error('Near stride-message router: ', e.name, ': ', e.message, ' logged at ', new Date().toJSON());
            
            res.statusCode = 400;
            res.send('Bad Request.');
        }
    }
);

// Send message to Stride -----------------------
function sendStrideRoomMessage(roomBearer, roomId, dataJson) {
    var clientServerOptions = {
        uri: strideApiUrl + '/site/' + strideSiteId + '/conversation/' + roomId + '/message',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+roomBearer
        },
        body: dataJson
    }
    request(clientServerOptions, function (error, response) {
        // console.log(error, response.body);
        log.info('Stride API answer: ', error,' - ', JSON.stringify(response.body), ' logged at ', new Date().toJSON());
        return;
    });
}

// Register our routes -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// Start the server
// =============================================================================
app.listen(port, () => {
  console.log('StrideDispatcher app started, listening on port '+port+'!');
});
