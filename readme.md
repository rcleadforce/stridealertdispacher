# Response Concepts' Stride Alert Dispatcher

This NodeJS app expects error messages in POST and forwards it to a ceratin Stride Room.  
For details check the Confluence documentation:
https://response-concepts.atlassian.net/wiki/spaces/DOC/pages/371425355/Stride+Chat+Alerts+and+Notifications

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Node JS: https://nodejs.org/en/
NPM: https://www.npmjs.com/
```

### Installing

A step by step:
1. Clone the code
```
git clone https://...
```
2. Install dpendencies
```
 npm install
```
3. Run
```
node index.js
```

## Sending messages

Accessed at POST http://{address}:8080/api/stride-message
Expected POST JSON data format: 
```
{ 
    "room": "roomname", 
    "level": "error-level", 
    "message": "message body" 
}
```
Room list
```
sandbox --> for test purpuses
systemalerts --> for PROD
```
Alert level list
```
notice
warning
error
critical 
```

## Config
The config file contains the PORT and Stride API related settings.

